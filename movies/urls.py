from django.conf.urls import url, include
from rest_framework import routers
from movies.views import MovieViewSet

router = routers.DefaultRouter()
router.register(r'movies', MovieViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
]
