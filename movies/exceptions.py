from rest_framework.views import exception_handler
from rest_framework.response import Response
from rest_framework import status


def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    # Now add the HTTP status code to the response.
    if not response:
        response = Response({"detail": exc.args[0]}, status=status.HTTP_400_BAD_REQUEST)
    return response


class CustomException(Exception):
    pass
