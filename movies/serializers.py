from rest_framework import serializers
from movies.models import Movie


# Serializers define the API representation.
class MovieSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Movie
        fields = ['id', 'name', 'year']
