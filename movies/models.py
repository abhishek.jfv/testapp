from django.db import models


class Movie(models.Model):
    name = models.CharField(max_length=198, null=False, blank=False)
    year = models.IntegerField(null=False, blank=False)

    def __str__(self):
        return "{} : {}".format(self.id, self.name)
