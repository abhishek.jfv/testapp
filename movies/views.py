from rest_framework import viewsets, permissions, response
from movies import permissions as custom_permissions
from movies.models import Movie
from movies.serializers import MovieSerializer
from django.shortcuts import get_object_or_404
from movies.exceptions import CustomException


class MovieViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated, custom_permissions.CustomPermission]
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer

    def retrieve(self, request, pk=None):
        if not pk.isnumeric():
            raise CustomException("Custom Exception: Pk is not a number")
        queryset = Movie.objects.all()
        user = get_object_or_404(queryset, pk=pk)
        serializer = MovieSerializer(user)
        return response.Response(serializer.data)
