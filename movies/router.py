class DemoRouter:
    """
    A router to control all database operations on models in the
    actor application.
    """

    def db_for_read(self, model, **hints):
        """
        Attempts to read actor models go to actors_db.
        """
        if model._meta.app_label == 'actors':
            return 'actors_db'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write actor models go to actors_db.
        """
        if model._meta.app_label == 'actors':
            return 'actors_db'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the actor app is involved.
        """
        if obj1._meta.app_label == 'actors' or \
                obj2._meta.app_label == 'actors':
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the auth app only appears in the 'actors_db'
        database.
        """
        if app_label == 'actors':
            return db == 'actors_db'
        return None
