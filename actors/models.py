from django.db import models


class Actor(models.Model):
    name = models.TextField(max_length=198)

    def __str__(self):
        return "{} : {}".format(self.id, self.name)
